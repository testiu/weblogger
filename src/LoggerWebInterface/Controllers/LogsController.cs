﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LoggerWebInterface.Models;
using LogRepository.Entities;
using LogRepository.Repositories;
using MongoDB.Bson;

namespace LoggerWebInterface.Controllers
{
    public class LogsController : Controller
    {
        //private IRepository<Log> db = new MongoDbRepository<Log>();

        // GET: /Logs/
        public ActionResult Index(FilterClass filterClass)
        {
            filterClass.Filter();

            if (Request.IsAjaxRequest())
            {
                return PartialView("_LogsTable", filterClass);
            }
            return View(filterClass);
        }

        public ActionResult Details(string id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }*/
            var filterClass = new FilterClass();
            var log = filterClass.GetLog(id);
            if (log == null)
            {
                return HttpNotFound();
            }
            return View(log);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
