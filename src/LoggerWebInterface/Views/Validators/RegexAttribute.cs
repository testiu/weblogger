﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace LoggerWebInterface.Views.Validators
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class RegexAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                Regex _regex = new Regex(Convert.ToString(value));
            }
            catch (Exception ex)
            {
                //ErrorMessage = ex.Message;
                return new ValidationResult(ex.Message);
            }
            return null;
        }
    }
}