﻿
var defaultInterval = 1000;
var intervalOfUpdate = defaultInterval;
var isUpdateChecked = true;

$(function() {

    var ajaxFormSubmit = function() {
        var $form = $(this);

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize()
        };

        $.ajax(options).done(function(data) {
            var $target = $($form.attr("data-otf-target"));
            $target.replaceWith(data);
        });

        return false;
    };

    $("form[data-otf-ajax='true']").submit(ajaxFormSubmit);

    var checkUpdate = function () {
        return $("#doUpdateOnInterval").is(':checked');
    };

    var getIntervalOfUpdate = function() {
        return $("#updateIntervalTime").prop('value');
    };

    var changeUpdateMode = function () {
        isUpdateChecked = checkUpdate();
        if (isUpdateChecked) {
            setTimerOn(intervalOfUpdate);
        }

        return false;
    };

    var changeIntervalOfUpdate = function () {
        var interval = getIntervalOfUpdate();
        if (interval > defaultInterval) {
            intervalOfUpdate = interval;
        } else {
            intervalOfUpdate = defaultInterval;
        }

        return false;
    };

    var setTimerOn = function (interval) {
        setTimeout(function () {
            if (isUpdateChecked) {
                $("#filterForm").submit();
                setTimerOn(intervalOfUpdate);
            }
            return false;
        }, interval);

        return false;
    };

    $("#doUpdateOnInterval").change(changeUpdateMode);
    $("#updateIntervalTime").change(changeIntervalOfUpdate);

    changeIntervalOfUpdate();
    changeUpdateMode();
});

