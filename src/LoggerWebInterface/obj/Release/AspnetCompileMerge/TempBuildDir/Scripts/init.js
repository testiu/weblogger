﻿$(function () {
    $(".date-time-value").datetimepicker({
        dateFormat: 'dd.mm.yy',
        timeFormat: 'HH:mm:ss'
    });

    $(document).ready(function () {
        $('.multiselect').multiselect();
    });
});