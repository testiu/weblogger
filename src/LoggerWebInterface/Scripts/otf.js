﻿
var defaultInterval = 1; // s
var intervalOfUpdate = defaultInterval;
var intervalOfHighlight = 4; // s
var isUpdateChecked = true;
var isSearchFormChanged = true;
var numRowsInTable = 100;
var timer = null;
var timeOfSleep = 500;
var freeToRequest = true;

$(function () {

    var ajaxFormSubmit = function() {
        if (freeToRequest) {
            freeToRequest = false;

            if (validateForm()) {
                spinnerLoadingOn();

                var $form = $(this);

                var options = {
                    url: $form.attr("action"),
                    type: $form.attr("method"),
                    data: $form.serialize()
                };
                console.log('ajaxsubmit');
                $.ajax(options).done(function(data) {
                    var $target = $($form.attr("data-otf-target"));

                    var root = $("<div>").attr('id', 'root').html(data);
                    var trs = $("tr", root);
                    trs.each(function() {
                        $(this).addClass("highlight-element");
                    });

                    if (isSearchFormChanged) {
                        $("#logTable").empty();
                        isSearchFormChanged = false;
                    }
                    $target.prepend(root.html());
                    highlightElements("#logTable");
                    deleteLastRows("#logTable", numRowsInTable);
                    spinnerLoadingOff();

                    if (isUpdateChecked) {
                        spinnerTimer(intervalOfUpdate);
                        setTimerOn(intervalOfUpdate);
                    }
                });
            } else {
                if (isUpdateChecked) {
                    spinnerTimer(intervalOfUpdate);
                    setTimerOn(intervalOfUpdate);
                }
            }

            
            freeToRequest = true;
        }
        return false;
    };

    $("form[data-otf-ajax='true']").submit(ajaxFormSubmit);

    var validateForm = function () {
        $("#regexError").text("");
        var isValid;
        try {
            new RegExp($("#messageRegex").val());
            isValid = true;
        }
        catch (e) {
            isValid = false;
        }

        if (!isValid)
            $("#regexError").text("Неправильное регулярное выражение");

        return isValid;
    };

    var datesUtcLocal = function (context) {
        var dates = $('.utc-date', context);

        dates.each(function () {
            //var m = moment($(this).html(), "DD-MM-YYYY HH:mm:ss Z");
            var m = moment($(this).html());
            //var s = m.format('LLLL');
            var s = m.format('DD.MM.YYYY, HH:mm:ss');
            $(this).html(s);
            $(this).removeClass("utc-date");
        });

    };

    var deleteLastRows = function (tableId, saveRowsCount) {
        var trs = $('tr', tableId);
        var rowCount = trs.length;
        var selector = "tr:eq(" + saveRowsCount.toString() + ")";
        for (var i = saveRowsCount + 1; i < rowCount; i++) {
            var tr = $(selector, tableId);
            tr.remove();
        }
    };
    var highlightElements = function (context) {
        var elNeedHighlight = $('.highlight-element', context);

        $(elNeedHighlight).each(function () {
            var row = $(this);
            row.removeClass('highlight-element');
            row.css("background", "#d4f870");
            row.animate({
                backgroundColor: "white"
            }, parseInt(intervalOfHighlight) * 1000, function() {});
        });

    };

    // Вариант с постепенным появлением
    var highlightElementsAppear = function (context) {
        var elNeedHighlight = $('.highlight-element', context);
        var curTime = 0;

        $(elNeedHighlight).each(function () {
            var row = $(this);
            row.removeClass('highlight-element');
            row.css("background", "#d4f870");
            row.addClass('hidden');
            setTimeout(function () {
                row.removeClass('hidden');
                row.animate({
                    backgroundColor: "white"
                }, parseInt(intervalOfHighlight) * 1000, function() {});
            }, curTime * 1000);
            curTime += intervalOfAppear;
        });

    };

    var checkUpdate = function() {
        return $("#doUpdateOnInterval").is(':checked');
    };

    var getIntervalOfUpdate = function() {
        return $("#updateIntervalTime").prop('value');
    };

    var changeUpdateMode = function() {
        isUpdateChecked = checkUpdate();
        if (isUpdateChecked) {
            console.log('changeupdate');
            adequateSubmit();
        } else {
            saveFilters();
            turnOffTimer();
        }
    };

    var changeIntervalOfUpdate = function() {
        var interval = getIntervalOfUpdate();
        if (interval > defaultInterval) {
            intervalOfUpdate = interval;
        } else {
            intervalOfUpdate = defaultInterval;
        }
    };

    var searchFormChanged = function () {
        isSearchFormChanged = true;
        return false;
    };

    var setTimerOn = function(interval) {
        timer = setTimeout(function() {
            if (isUpdateChecked) {
                console.log('timeout');
                adequateSubmit();
            }
        }, interval*1000);

        return false;
    };

    var adequateSubmit = function () {
        console.log('adequate');
        if (!isSearchFormChanged)
            $("#maxObjectId").val(findMaxObjId());
        else
            $("#maxObjectId").val("");

        $("#filterForm").submit();

        // save cookies
        saveFilters();
    };

    var turnOffTimer = function() {
        if (timer !== null)
            clearTimeout(timer);
        $('#spinner-timer').empty();
    };

    var manualSubmit = function () {
        turnOffTimer();
        console.log('manual');
        adequateSubmit();
        setTimerOn();
    };

    var findMaxObjId = function (tableId) {
        var trs = $('tr #ObjIdCreateTime', tableId);
        var max = undefined;
        var maxEl = undefined;
        trs.each(function () {
            if (max === undefined || $(this).val() > max) {
                max = $(this).val();
                maxEl = $(this).parent();
            }
        });

        var objId = $('#ObjId', maxEl).val();
        return objId;
    };

    var saveFilters = function() {
        var values = {};
        values['fromTime'] = $('#fromTime').val();
        values['toTime'] = $('#toTime').val();
        values['levelMultiselect'] = getMultipleSelectVals("levelMultiselect");
        values['hostMultiselect'] = getMultipleSelectVals('hostMultiselect');
        values['processMultiselect'] = getMultipleSelectVals('processMultiselect');
        values['isUpdateChecked'] = checkUpdate();
        values['intervalOfUpdate'] = $('#updateIntervalTime').val();
        values['messageRegex'] = $('#messageRegex').val();

        var json_str = JSON.stringify(values);
        setCookie('filter-data', json_str);
    };

    var loadFilters = function() {
        var jsonStr = getCookie('filter-data');
        if (jsonStr !== undefined) {
            var obj = JSON.parse(jsonStr);
            $('#fromTime').val(obj['fromTime']);
            $('#toTime').val(obj['toTime']);
            $('#levelMultiselect').multiselect('select', obj['levelMultiselect']);
            $('#hostMultiselect').multiselect('select', obj['hostMultiselect']);
            $('#processMultiselect').multiselect('select', obj['processMultiselect']);
            $('#doUpdateOnInterval').prop('checked', obj['isUpdateChecked']);
            $('#updateIntervalTime').val(obj['intervalOfUpdate']);
            $('#messageRegex').val(obj['messageRegex']);
        }
       
    };

    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    // возвращает cookie с именем name, если есть, если нет, то undefined
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    var getMultipleSelectVals = function(id) {
        var vals = [];
        var textvals = [];

        $('#' + id + ' :selected').each(function(i, selected) {
            vals[i] = $(selected).val();
            textvals[i] = $(selected).text();
        });
        return vals;
    };

    var spinnerLoadingOn = function() {
        $('#spinner-timer').addClass('hidden');
        $('#spinner-load').removeClass('hidden');
    };

    var spinnerLoadingOff = function() {
        $('#spinner-load').addClass('hidden');
    };

    var spinnerTimer = function (count) {
        console.log('spinner');
        $('#spinner-timer').removeClass('hidden');
        $('#spinner-timer').pieChartCountDown({
            time: 5,
            color: 'gray',
            background: 'white',
            //TODO: возможно заменить SetTimer на опцию callback: function(){...} здесь
            size: 15,
            infinite: false,
            border: 0
        });
    };

    var trimContent = function () {
        console.log("click - enter trim function");
        if ($(this).hasClass('trimmed-content')) {
            $(this).removeClass('trimmed-content').addClass('not-trimmed-content');
        } else {
            $(this).removeClass('not-trimmed-content').addClass('trimmed-content');
        }
    };


    $("#doUpdateOnInterval").change(changeUpdateMode);
    $("#updateIntervalTime").change(changeIntervalOfUpdate);
    $("#filterForm input:not(#doUpdateOnInterval #updateIntervalTime #searchSubmitBtn), #filterForm select").change(searchFormChanged);
    $("#searchSubmitBtn").click(manualSubmit);
    $(".can-trim-content").click(trimContent);

    setTimeout(function() {
        loadFilters();
    }, 0);
    setTimeout(function () {
        changeIntervalOfUpdate();
        changeUpdateMode();
    }, 2);
});