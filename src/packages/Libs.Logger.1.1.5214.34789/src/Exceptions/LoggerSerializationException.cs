﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libs.Logger.Exceptions
{
    /// <summary>
    /// Ошибка конфигации логгера.
    /// </summary>
    [Serializable]
    public class LoggerSerializationException : LoggerException
    {
        public LoggerSerializationException(string message) : base(message) { }
        public LoggerSerializationException(string message, Exception innerException) : base(message, innerException) { }
    }
}
