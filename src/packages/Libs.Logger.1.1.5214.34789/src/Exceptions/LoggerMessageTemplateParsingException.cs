﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libs.Logger.Exceptions
{
    public class LoggerMessageTemplateParsingException : LoggerConfigurationException
    {
        public LoggerMessageTemplateParsingException(string message) : base(message) { }
        public LoggerMessageTemplateParsingException(string message, Exception innerException) : base(message, innerException) { }
    }
}
