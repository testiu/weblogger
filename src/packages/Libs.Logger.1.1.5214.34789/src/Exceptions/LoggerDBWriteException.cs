﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Libs.Logger.Exceptions
{
    /// <summary>
    /// Ошибка записи лога в базу данных.
    /// </summary>
    [Serializable]
    public class LoggerDBWriteException : LoggerException
    {
        public LoggerDBWriteException(string message) : base(message) { }
        public LoggerDBWriteException(string message, Exception innerException) : base(message, innerException) { }
    }
}
