﻿using Libs.Logger.Common;


namespace Libs.Logger.Net
{
    /// <summary>
    /// Реализация интерфейса INetService.
    /// Принимает входящее сообщение, отправляет вызывающей стороне кол-во принятых байт
    /// </summary>
    public abstract class NetService : INetService
    {
        /// <summary>
        /// Передаем данные - получаем количество принятых на той стороне байт
        /// </summary>
        /// <param name="data">Массив байт</param>
        public void SendData(byte[] data)
        {
            if (data == null)
                return;

            LoggingEvent msg = null;
            try
            {
                msg = Libs.Logger.Helpers.Serializer.Deserialize<LoggingEvent>(data);
            }
            catch (Libs.Logger.Exceptions.LoggerSerializationException)
            {
                throw new System.ServiceModel.FaultException("Bad message format. Can't deserialize.");
            }
            OnDataRecieved(msg);
        }

        /// <summary>
        /// Вызов события у подписчиков
        /// </summary>
        protected abstract void OnDataRecieved(LoggingEvent data);
    }
}
