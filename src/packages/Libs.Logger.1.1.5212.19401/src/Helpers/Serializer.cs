﻿

//  old Serializers
//using Newtonsoft.Json;
//using Newtonsoft.Json.Bson;

//using System.Runtime.Serialization.Json;
//using fastBinaryJSON;
//using System.Runtime.Serialization.Json;

using System;
using System.IO;
using Libs.Logger.Exceptions;

namespace Libs.Logger.Helpers
{
    /// <summary>
    /// Сериализатор
    /// </summary>
    internal static class Serializer
    {
        #region protobuf

        /// <summary>
        /// Сериализация объекта в бинарный вид
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="obj">Объект</param>
        /// <returns></returns>
        public static byte[] Serialize<T>(T obj)
        {
            var ms = new MemoryStream();
            ProtoBuf.Serializer.Serialize<T>(ms, obj);

            byte[] src = ms.ToArray();
            byte[] dist = new byte[(int) ms.Length];

            Buffer.BlockCopy(src, 0, dist, 0, (int)ms.Length);

            return dist;
        }

        /// <summary>
        /// Сериализация с указание длины объекта в первых 4 байтах (для передачи по сети)
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="obj">Объект</param>
        /// <returns></returns>
        public static byte[] SerializeWithLength<T>(T obj)
        {
            var ms = new MemoryStream();

            ms.Position = 4;
            ProtoBuf.Serializer.Serialize(ms, obj);

            int length = (int) ms.Position - 4;
            ms.Position = 0;
            ms.Write(BitConverter.GetBytes(length), 0, 4);

            byte[] src = ms.ToArray();
            byte[] dist = new byte[(int)ms.Length];

            Buffer.BlockCopy(src, 0, dist, 0, (int)ms.Length);

            return dist;
        }

        /// <summary>
        /// Десериализация
        /// </summary>
        /// <typeparam name="T">Тип</typeparam>
        /// <param name="buffer">Набор байт</param>
        /// <returns></returns>
        public static T Deserialize<T>(byte[] buffer)
        {
            try
            {
                var ms = new MemoryStream(buffer);
                T body = ProtoBuf.Serializer.Deserialize<T>(ms);

                return body;
            }
            catch (ProtoBuf.ProtoException ex)
            {
                throw new LoggerSerializationException("Ошибка при десериализации.", ex);
            }
        }

        #endregion

        #region FastBinaryJSON

        //public static byte[] Serialize(LoggingEvent data)
        //{
        //    var bytes = BJSON.Instance.ToBJSON(data);

        //    return bytes;
        //}

        //public static LoggingEvent Deserialize(byte[] bytes)
        //{
        //    var data = BJSON.Instance.ToObject<LoggingEvent>(bytes);

        //    return data;
        //}

        #endregion

        #region JSON

        //private static readonly JsonSerializer Serializer = new JsonSerializer();

        //public static string SerializeToJsonString(object objectToSerialize)
        //{
        //    using (var ms = new MemoryStream())
        //    {
        //        var serializer =
        //                new DataContractJsonSerializer(objectToSerialize.GetType());
        //        serializer.WriteObject(ms, objectToSerialize);
        //        ms.Position = 0;

        //        using (var reader = new StreamReader(ms))
        //        {
        //            return reader.ReadToEnd();
        //        }
        //    }
        //}

        //public static LoggingEvent DeserializeData(string jsonData)
        //{
        //    using (var ms = new MemoryStream(Encoding.Unicode.Serialize(jsonData)))
        //    {
        //        var serializer =
        //                new DataContractJsonSerializer(typeof(LoggingEvent));

        //        return (LoggingEvent)serializer.ReadObject(ms);
        //    }
        //}


        //public static byte[] Serialize<T>(T obj)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    Serializer.Serialize<T>(ms, obj);
        //    byte[] body = ms.GetBuffer();
        //    Buffer.BlockCopy(body, 0, body, 0, (int)ms.Length);
        //    return body;
        //}

        //public static T Deserialize<T>(byte[] buffer)
        //{
        //    MemoryStream ms = new MemoryStream(buffer);
        //    T body = Serializer.Deserialize<T>(ms);
        //    return body;
        //}

        //public static byte[] SerializeToJson(LoggingEvent msg)
        //{
        //    var stream = new MemoryStream();
        //    var writer = new BsonWriter(stream);

        //    Serializer.Serialize(writer, msg);
        //    byte[] bodyBytes = stream.GetBuffer();

        //    int length = (int)stream.Length;
        //    byte[] lengthBytes = BitConverter.Serialize(length);

        //    var result = new byte[length + lengthBytes.Length];
        //    Buffer.BlockCopy(lengthBytes, 0, result, 0, lengthBytes.Length);
        //    Buffer.BlockCopy(bodyBytes, 0, result, lengthBytes.Length, (int)stream.Length);

        //    return result;
        //}

        #endregion
    }
}
