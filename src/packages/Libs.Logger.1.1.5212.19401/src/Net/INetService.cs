﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Libs.Logger.Net
{
    /// <summary>
    /// Простой интерфейс для передачи данных по сети
    /// </summary>
    [ServiceContract]
    public interface INetService
    {
        /// <summary>
        /// Передаем данные на сервер. 
        /// Предполагается что под массивом байт может пониматься любой сериализованный объект.
        /// </summary>
        /// <param name="data">Массив байт</param>
        [OperationContract(IsOneWay = true)]
        void SendData(byte[] data);
    }
}
