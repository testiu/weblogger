﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Libs.Logger.Net
{
    public class StableLoggerNetClient: INetService, IDisposable
    {
        public static StableLoggerNetClient CreateOnTcp(string address, int port, string serviceName = "LoggingService")
        {
            EndpointAddress addr = new EndpointAddress(string.Format("net.tcp://{0}:{1}/{2}", address, port, serviceName));
            var binding = new NetTcpBinding(SecurityMode.None);

            return new StableLoggerNetClient(binding, addr, 2000);
        }

        public static StableLoggerNetClient CreateOnPipe(string address, string pipeName = "LoggingService")
        {
            EndpointAddress addr = new EndpointAddress(string.Format("net.pipe://{0}/{1}", address, pipeName));
            var binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);

            return new StableLoggerNetClient(binding, addr, 2000);
        }


        // =======================

        private string _crEnpointConfigName;
        private System.ServiceModel.EndpointAddress _crRemoteAddr;
        private System.ServiceModel.Channels.Binding _crBinding;

        private LoggerNetClient _curClient;

        private Thread _connectToUpdateAPIThread;
        private CancellationTokenSource _procStopTokenSource = new CancellationTokenSource();
        private object _syncObj = new object();

        private int _connectionTestTimeMs;


        public StableLoggerNetClient(string endpointConfigurationName, int connectionTestTimeMs)
        {
            _crEnpointConfigName = endpointConfigurationName;

            _connectionTestTimeMs = connectionTestTimeMs;
        }

        public StableLoggerNetClient(string endpointConfigurationName, string remoteAddress, int connectionTestTimeMs) 
        {
            _crEnpointConfigName = endpointConfigurationName;
            _crRemoteAddr = new System.ServiceModel.EndpointAddress(remoteAddress);

            _connectionTestTimeMs = connectionTestTimeMs;
        }

        public StableLoggerNetClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress, int connectionTestTimeMs)
        {
            _crEnpointConfigName = endpointConfigurationName;
            _crRemoteAddr = remoteAddress;

            _connectionTestTimeMs = connectionTestTimeMs;
        }

        public StableLoggerNetClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress, int connectionTestTimeMs)
        {
            _crBinding = binding;
            _crRemoteAddr = remoteAddress;

            _connectionTestTimeMs = connectionTestTimeMs;
        }


        public string RemoteSideName
        {
            get
            {
                if (_crRemoteAddr != null)
                    return _crRemoteAddr.Uri.ToString();

                return _crEnpointConfigName;
            }
        }


        public bool HasConnection
        {
            get { return _curClient != null && _curClient.State == System.ServiceModel.CommunicationState.Opened; }
        }

        public bool IsStarted
        {
            get
            {
                return _connectToUpdateAPIThread != null;
            }
        }

        protected virtual void LogError(Exception ex, string message)
        {

        }
        protected virtual void LogWarn(Exception ex, string message)
        {

        }


        void INetService.SendData(byte[] data)
        {
            SendData(data);
        }

        public bool SendData(Libs.Logger.Common.LoggingEvent data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            var byteArr = Libs.Logger.Helpers.Serializer.Serialize(data);
            return SendData(byteArr);
        }

        public bool SendData(byte[] data)
        {
            if (_curClient == null || _curClient.State != System.ServiceModel.CommunicationState.Opened)
            {
                LogWarn(null, "Can't send data to Log Server: " + RemoteSideName + ". Connection is not established.");
                return false;
            }

            lock (_syncObj)
            {
                if (_curClient == null || _curClient.State != System.ServiceModel.CommunicationState.Opened)
                    return false;

                _curClient.RemoteSide.SendData(data);
            }

            return true;
        }


        public void Start()
        {
            if (_connectToUpdateAPIThread != null)
                throw new InvalidOperationException("UpdateAPIStableClient is already started");

            _procStopTokenSource = new CancellationTokenSource();

            _connectToUpdateAPIThread = new Thread(UpdateAPIConnectingThreadFunc);
            _connectToUpdateAPIThread.Name = "Anicllary connection thread";

            _connectToUpdateAPIThread.Start();
        }

        public void Stop()
        {
            if (_connectToUpdateAPIThread == null)
                return;


            _procStopTokenSource.Cancel();

            if (_connectToUpdateAPIThread != null)
                _connectToUpdateAPIThread.Join();

            lock (_syncObj)
            {
                if (_curClient != null && _curClient.State != System.ServiceModel.CommunicationState.Closed)
                {
                    if (_curClient.State != System.ServiceModel.CommunicationState.Opened)
                    {
                        try
                        {
                            _curClient.Abort();
                        }
                        catch { }
                    }

                    try
                    {
                        _curClient.Close();
                    }
                    catch { }

                    if (_curClient.State != System.ServiceModel.CommunicationState.Closed)
                    {
                        try
                        {
                            _curClient.Abort();
                            _curClient.Close();
                        }
                        catch { }
                    }
                }
                _curClient = null;
            }
        }






        private LoggerNetClient CreateNewClient()
        {
            if (_crBinding != null)
                return new LoggerNetClient(_crBinding, _crRemoteAddr);

            if (_crRemoteAddr != null)
                return new LoggerNetClient(_crEnpointConfigName, _crRemoteAddr);

            return new LoggerNetClient(_crEnpointConfigName);
        }



        private void UpdateAPIConnectingThreadFunc()
        {
            var token = _procStopTokenSource.Token;

            bool wasErrorPrinted = false;
            try
            {
                while (!token.IsCancellationRequested)
                {
                    if (_curClient == null || _curClient.State != System.ServiceModel.CommunicationState.Opened)
                    {
                        lock (_syncObj)
                        {
                            if (_curClient != null)
                            {
                                try
                                {
                                    _curClient.Abort();
                                    _curClient.Close();
                                }
                                catch { }
                                _curClient = null;
                            }

                            _curClient = CreateNewClient();
                            try
                            {
                                _curClient.Open();
                                wasErrorPrinted = false;
                            }
                            catch (TimeoutException tmExc)
                            {
                                if (!wasErrorPrinted)
                                {
                                    LogError(tmExc, "Can't connect to log server: " + RemoteSideName);
                                    wasErrorPrinted = true;
                                }
                            }
                            catch (CommunicationException cmExc)
                            {
                                if (!wasErrorPrinted)
                                {
                                    LogError(cmExc, "Can't connect to log server: " + RemoteSideName);
                                    wasErrorPrinted = true;
                                }
                            }
                        }
                    }

                    token.WaitHandle.WaitOne(_connectionTestTimeMs);
                }
            }
            catch (OperationCanceledException cex)
            {
                if (!token.IsCancellationRequested)
                {
                    LogError(cex, "Unknown error inside connecting to log server: " + RemoteSideName);
                    throw;
                }
            }
            catch (Exception ex)
            {
                LogError(ex, "Unknown error inside connecting to log server: " + RemoteSideName);
                throw;
            }
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
