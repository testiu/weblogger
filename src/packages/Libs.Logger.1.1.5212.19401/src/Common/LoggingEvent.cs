﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Libs.Logger.Common
{
    /// <summary>
    /// Контейнер, хранящий информацию о сообщении, которое хотим залогировать
    /// </summary>
    [DataContract]
    public class LoggingEvent
    {
        /// <summary>
        /// Констуктор для создания логгирующего сообщения (нужен для десериализации)
        /// </summary>
        protected internal LoggingEvent()
        {
        }

        /// <summary>
        /// Констуктор для создания логгирующего сообщения
        /// </summary>
        /// <param name="date"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="level"></param>
        /// <param name="context"></param>
        /// <param name="stackSources"></param>
        /// <param name="class"></param>
        /// <param name="method"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public LoggingEvent(DateTime date, string message, Exception exception, LogLevel level, string context,
                            List<string> stackSources, string @class = null, string method = null,
                            string filePath = null, int lineNumber = -1)
        {
            Date = date;
            Message = message;
            Level = level;
            Context = context;
            StackSources = stackSources;
            Clazz = @class;
            Method = method;
            FilePath = filePath;
            LineNumber = lineNumber;

            if (exception != null)
                Exception = new Error(exception);
        }

        /// <summary>
        /// Констуктор для создания логгирующего сообщения
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="level"></param>
        /// <param name="context"></param>
        /// <param name="stackSources"></param>
        /// <param name="class"></param>
        /// <param name="method"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public LoggingEvent(string message, Exception exception, LogLevel level, string context,
                            List<string> stackSources, string @class = null, string method = null, string filePath = null,
                            int lineNumber = -1)
        {
            Date = DateTime.Now;
            Message = message;
            Level = level;
            Context = context;
            StackSources = stackSources;
            Clazz = @class;
            Method = method;
            FilePath = filePath;
            LineNumber = lineNumber;

            if (exception != null)
                Exception = new Error(exception);
        }

        /// <summary>
        /// Констуктор для создания логгирующего сообщения
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="level"></param>
        /// <param name="stackSources"></param>
        /// <param name="class"></param>
        /// <param name="method"></param>
        /// <param name="filePath"></param>
        /// <param name="lineNumber"></param>
        public LoggingEvent(string message, Exception exception, LogLevel level,
                            List<string> stackSources, string @class = null, string method = null, string filePath = null,
                            int lineNumber = -1)
        {
            Date = DateTime.Now;
            Message = message;
            Level = level;
            StackSources = stackSources;
            Clazz = @class;
            Method = method;
            FilePath = filePath;
            LineNumber = lineNumber;

            if (exception != null)
                Exception = new Error(exception);
        }

        #region properties
        
        /// <summary>
        /// Дата и время создания логирующего сообщения
        /// </summary>
        [DataMember(Order = 1)]
        public DateTime Date { get; private set; }

        /// <summary>
        /// Уровень логирования
        /// </summary>
        [DataMember(Order = 2)]
        public LogLevel Level { get; private set; }

        /// <summary>
        /// Контекст логируемого сообщения
        /// для упрощения поиска и отслещивания лога от отдельных подсистем или для определенных процессов
        /// </summary>
        /// <summary>
        /// Пример использования - "id:12,name:NetSystem"
        /// "id:Arm13,task:24,cam:3"
        /// </summary>>
        [DataMember(Order = 3)]
        public string Context { get; private set; }

        /// <summary>
        /// Полное имя класса которое мы хотим вывести в логе, т.е. Namespace.Class
        /// Важное замечание: для чтения лога с помощью программы LogReader обязательно должна присутствовать
        /// и левая и правая часть имени через точку - иначе программа вылетает
        /// </summary>
        [DataMember(Order = 4)]
        public string Clazz { get; private set; }

        /// <summary>
        /// Имя метода из которого было вызвано логирование
        /// </summary>
        [DataMember(Order = 5)]
        public string Method { get; private set; }

        /// <summary>
        /// Полное имя файла исходного кода, в котором было вызвано логирование
        /// </summary>
        [DataMember(Order = 6)]
        public string FilePath { get; private set; }

        /// <summary>
        /// Номер строки в файле исходного кода, в которой было вызвано логирование
        /// </summary>
        [DataMember(Order = 7)]
        public int LineNumber { get; private set; }

        /// <summary>
        /// Логируемый текст, сообщение
        /// </summary>
        [DataMember(Order = 8)]
        public string Message { get; private set; }

        /// <summary>
        /// Иформация об исключительной информации
        /// </summary>
        [DataMember(Order = 9)]
        public Error Exception { get; private set; }

        /// <summary>
        /// Список имен модулей в порядке иерархии.
        /// Внутренний логгеры первый в списке, внешний - последний
        /// </summary>
        [DataMember(Order = 10)]
        public List<string> StackSources { get; private set; }

        #endregion
    }
}