﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libs.Logger.Exceptions
{
    /// <summary>
    /// Ошибка записи лога в файл.
    /// </summary>
    [Serializable]
    public class LoggerFileWriteException : LoggerException
    {
        public LoggerFileWriteException(string message) : base(message) { }
        public LoggerFileWriteException(string message, Exception innerException) : base(message, innerException) { }
    }
}
