﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libs.Logger.Exceptions
{
    /// <summary>
    /// Ошибка передачи лога по сети.
    /// </summary>
    [Serializable]
    public class LoggerNetWriteException : LoggerException
    {
        public LoggerNetWriteException(string message) : base(message) { }
        public LoggerNetWriteException(string message, Exception innerException) : base(message, innerException) { }
    }
}
