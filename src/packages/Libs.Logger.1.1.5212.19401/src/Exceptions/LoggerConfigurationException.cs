﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Libs.Logger.Exceptions
{
    /// <summary>
    /// Ошибка конфигации логгера.
    /// </summary>
    [Serializable]
    public class LoggerConfigurationException : LoggerException
    {
        public LoggerConfigurationException(string message) : base(message) { }
        public LoggerConfigurationException(string message, Exception innerException) : base(message, innerException) { }
    }
}
