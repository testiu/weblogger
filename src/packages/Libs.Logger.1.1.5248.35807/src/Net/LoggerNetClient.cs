﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Libs.Logger.Net
{
    public class LoggerNetClient: System.ServiceModel.ClientBase<INetService>
    {
        public LoggerNetClient(string endpointConfigurationName) :
            base(endpointConfigurationName)
        {
        }

        public LoggerNetClient(string endpointConfigurationName, string remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public LoggerNetClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public LoggerNetClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        public event EventHandler Faulted
        {
            add { (this as ICommunicationObject).Faulted += value; }
            remove { (this as ICommunicationObject).Faulted -= value; }
        }


        public INetService RemoteSide
        {
            get
            {
                return this.Channel;
            }
        }
    }
}
