﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Libs.Logger.Net
{
    public class LoggerServer : IDisposable
    {
        public static LoggerServer Create(INetService singleton)
        {
            ServiceHost host = new ServiceHost(singleton);

            return new LoggerServer(host);
        }

        public static LoggerServer CreateOnTcp(INetService singleton, int port, string serviceName = "LoggingService")
        {
            Uri baseAddr = new Uri(string.Format("net.tcp://0.0.0.0:{0}/{1}", port, serviceName));
            ServiceHost host = new ServiceHost(singleton, baseAddr);

            var binding = new NetTcpBinding(SecurityMode.None);
            host.AddServiceEndpoint(typeof(INetService), binding, baseAddr);

            var behavior = host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
            behavior.InstanceContextMode = InstanceContextMode.Single;

            var debugBehavior = host.Description.Behaviors.Find<System.ServiceModel.Description.ServiceDebugBehavior>();

            if (debugBehavior != null)
                debugBehavior.IncludeExceptionDetailInFaults = true;

            return new LoggerServer(host);
        }


        public static LoggerServer CreateOnPipe(INetService singleton, string pipeName = "LoggingService")
        {
            Uri baseAddr = new Uri(string.Format("net.pipe://localhost/{0}", pipeName));
            ServiceHost host = new ServiceHost(singleton, baseAddr);

            var binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None);
            host.AddServiceEndpoint(typeof(INetService), binding, baseAddr);

            var behavior = host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
            behavior.InstanceContextMode = InstanceContextMode.Single;

            var debugBehavior = host.Description.Behaviors.Find<System.ServiceModel.Description.ServiceDebugBehavior>();

            if (debugBehavior != null)
                debugBehavior.IncludeExceptionDetailInFaults = true;

            return new LoggerServer(host);
        }


        private ServiceHost _host;

        protected LoggerServer(ServiceHost host)
        {
            if (host == null)
                throw new ArgumentNullException("host");

            _host = host;
        }


        public void Open()
        {
            _host.Open();
        }


       

        public void Dispose()
        {
            if (_host != null)
            {
                if (_host.State != CommunicationState.Closed)
                {
                    try
                    {
                        _host.Close();
                    }
                    catch
                    {
                        _host.Abort();
                    }
                }
                _host = null;
            }
        }
    }
}
