﻿using System.Linq;
using Libs.Logger.Common;
using Libs.Logger.Configuration;
using Libs.Logger.Initializer;
using Libs.Logger.LoggingEventConverters;
using Libs.Logger.Writers;
using System;
using System.Diagnostics.Contracts;

namespace Libs.Logger
{
    /// <summary>
    /// Логгер для использования в подсистемах
    /// Желательно оборачивать в свой собственный сингелтон в подсистеме, 
    /// который обязательно наследовать от данного класса.
    /// </summary>
    public class Logger : LoggerBase
    {
        private static Logger _emptyInstance;
        private static Logger _consoleInstance;
        private static Logger _defaultInstance;

        private static readonly object _lockCreation = new object();

        /// <summary>
        /// Пустой логгер (в никуда)
        /// </summary>
        public static Logger EmptyLogger
        {
            get
            {
                if (_emptyInstance == null)
                {
                    lock (_lockCreation)
                    {
                        if (_emptyInstance == null)
                        {
                            var config = new EmptyWriterConfiguration();
                            _emptyInstance = new Logger(LogLevel.Debug, "EmptyLogger", LoggerFactory.CreateWriter(config), false, true);
                        }
                    }
                }

                return _emptyInstance;
            }
        }

        /// <summary>
        /// Простой консольный логгер
        /// </summary>
        public static Logger ConsoleLogger
        {
            get
            {
                if (_consoleInstance == null)
                {
                    lock (_lockCreation)
                    {
                        if (_consoleInstance == null)
                        {
                            var config = new ConsoleWriterConfiguration();
                            _consoleInstance = new Logger(LogLevel.FullLog, "ConsoleLogger", LoggerFactory.CreateWriter(config), false, true);
                        }
                    }
                }

                return _consoleInstance;
            }
        }

        /// <summary>
        /// Логгер по умолчанию
        /// </summary>
        public static Logger Default
        {
            get
            {
                if (_defaultInstance == null)
                    System.Threading.Interlocked.CompareExchange(ref _defaultInstance, ConsoleLogger, null);
                return _defaultInstance;
            }
        }

        /// <summary>
        /// Задать логгер по умолчанию
        /// </summary>
        /// <param name="newDefault">Новый логгер</param>
        public static void SetDefault(Logger newDefault)
        {
            if (newDefault == null)
                newDefault = ConsoleLogger;

            var oldLogger = System.Threading.Interlocked.Exchange(ref _defaultInstance, newDefault);
            if (oldLogger != null && oldLogger != ConsoleLogger && oldLogger != EmptyLogger)
                oldLogger.Dispose();
        }
        /// <summary>
        /// Сбросить логгер по умолчанию в стандартный
        /// </summary>
        public static void ResetDefault()
        {
            SetDefault(null);
        }
        /// <summary>
        /// Загрузить логгер по умолчанию из файла конфигурации
        /// </summary>
        /// <param name="sectionName">Имя секции для загрузки</param>
        public static void LoadDefaultFromAppConfig(string sectionName = "LoggerConfigurationSection")
        {
            Contract.Requires<ArgumentNullException>(sectionName != null);

            var logger = LoggerFactory.CreateLoggerFromAppConfig("Default", sectionName);
            SetDefault(logger);
        }


        /// <summary>
        /// Инициализировать обёртку логгера
        /// </summary>
        /// <param name="assembly">Сборка для поиска</param>
        /// <param name="wrapper">Обёртка</param>
        /// <returns>Количество инициализированных логгеров</returns>
        public static void InitializeLoggerInAssembly(ILogger wrapper, System.Reflection.Assembly assembly)
        {
            Contract.Requires<ArgumentNullException>(wrapper != null);
            Contract.Requires<ArgumentNullException>(assembly != null);

            InitializerHelper.InitializeLoggerWrappers(wrapper, assembly);
        }

        /// <summary>
        /// Инициализировать логгеры в других сборках как дочерние
        /// </summary>
        /// <param name="wrapper">Обёртка</param>
        /// <param name="types">Произвольные типы из сборок с дочерними логгерами</param>
        /// <returns>Количество инициализированных логгеров</returns>
        public static void InitializeLoggersInAssemblies(ILogger wrapper, params Type[] types)
        {
            Contract.Requires<ArgumentNullException>(wrapper != null);
            Contract.Requires<ArgumentNullException>(types != null);

            wrapper.WrapLoggersInAssemblies(types);
        }


        /// <summary>
        /// Инициализировать обёртку логгера
        /// </summary>
        /// <param name="wrapper">Обёртка</param>
        /// <param name="assembly">Сборки для поиска</param>
        /// <returns>Количество инициализированных логгеров</returns>
        public static void InitializeLoggerInAssembly(ILogger wrapper, System.Collections.Generic.IEnumerable<System.Reflection.Assembly> assembly)
        {
            Contract.Requires<ArgumentNullException>(wrapper != null);
            Contract.Requires<ArgumentNullException>(assembly != null);

            InitializerHelper.InitializeLoggerWrappers(wrapper, assembly);
        }


        /// <summary>
        /// Определяет, включён ли логгер (для well-known логгеров)
        /// </summary>
        /// <param name="logger">Интерфейс логгера</param>
        /// <returns>Включён ли</returns>
        private static bool DetectIsEnabled(ILogger logger)
        {
            if (logger == null)
                return false;

            var loggerBase = logger as LoggerBase;
            if (loggerBase == null)
                return true;

            return loggerBase.IsLoggerEnabled;
        }


        /// <summary>
        /// Создание логгера
        /// </summary>
        /// <param name="logLevel">Уровень логирования</param>
        /// <param name="moduleName">Имя модуля (подсистемы)</param>
        /// <param name="innerLogger">Внутренний логгер</param>
        /// <param name="enableStackTraceExtraction">Разрешено ли получать данные из StackTrace</param>
        /// <param name="isEnabled">Вклюён ли логгер</param>
        internal Logger(LogLevel logLevel, string moduleName, ILoggingEventWriter innerLogger, bool enableStackTraceExtraction, bool isEnabled)
            : base(logLevel, moduleName, innerLogger, enableStackTraceExtraction, isEnabled)
        {
        }

        /// <summary>
        /// Создание логгера
        /// </summary>
        /// <param name="logLevel">Уровень логирования</param>
        /// <param name="moduleName">Имя модуля (подсистемы)</param>
        /// <param name="typeInfo">Тип, к которому привзяан логгер</param>
        /// <param name="innerLogger">Внутренний логгер</param>
        /// <param name="enableStackTraceExtraction">Разрешено ли получать данные из StackTrace</param>
        /// <param name="isEnabled">Вклюён ли логгер</param>
        internal Logger(LogLevel logLevel, string moduleName, Type typeInfo, ILoggingEventWriter innerLogger, bool enableStackTraceExtraction, bool isEnabled)
            : base(logLevel, moduleName, typeInfo, innerLogger, enableStackTraceExtraction, isEnabled)
        {
        }


        /// <summary>
        /// Создание логгера
        /// </summary>
        /// <param name="moduleName">Имя модуля (подсистемы)</param>
        /// <param name="innerLogger">Внутренний логгер</param>
        protected Logger(string moduleName, ILogger innerLogger)
            : base(innerLogger.Level, moduleName, innerLogger, innerLogger.AllowStackTraceInfoExtraction, DetectIsEnabled(innerLogger))
        {
        }

        /// <summary>
        /// Создание логгера
        /// </summary>
        /// <param name="logLevel">Уровень логирования</param>
        /// <param name="moduleName">Имя модуля (подсистемы)</param>
        /// <param name="innerLogger">Внутренний логгер</param>
        protected Logger(LogLevel logLevel, string moduleName, ILogger innerLogger)
            : base(logLevel, moduleName, innerLogger, innerLogger.AllowStackTraceInfoExtraction, DetectIsEnabled(innerLogger))
        {
        }




        /// <summary>
        /// Получение логгера для указанного типа
        /// </summary>
        /// <param name="typeInfo">Тип</param>
        /// <returns>Логгер для типа</returns>
        public Logger GetClassLogger(Type typeInfo)
        {
            if (typeInfo == null)
                throw new ArgumentNullException("typeInfo");

            return new Logger(this.Level, this.ModuleName, typeInfo, this, this.AllowStackTraceInfoExtraction, this.IsLoggerEnabled);
        }

        /// <summary>
        /// Получение логгера для текущего класса
        /// </summary>
        /// <returns>Логгер для типа</returns>
        public Logger GetThisClassLogger()
        {
            var stack = new System.Diagnostics.StackTrace(false);
            for (int i = 0; i < stack.FrameCount; i++)
            {
                var frame = stack.GetFrame(i);
                var curMInf = frame.GetMethod();
                if (curMInf != null && curMInf.DeclaringType != typeof(LoggerBase) && !curMInf.DeclaringType.IsSubclassOf(typeof(LoggerBase)))
                {
                    return GetClassLogger(curMInf.DeclaringType);
                }
            }

            return this;
        }
    }
}