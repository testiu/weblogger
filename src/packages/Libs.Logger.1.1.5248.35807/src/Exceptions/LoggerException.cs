﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Libs.Logger
{
    /// <summary>
    /// Базовый класс для исключений бросаемых логгером
    /// </summary>
    [Serializable]
    public class LoggerException : ApplicationException
    {
        public LoggerException() { }
        public LoggerException(string message) : base(message) { }
        public LoggerException(string message, Exception innerException) : base(message, innerException) { }
        protected LoggerException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
