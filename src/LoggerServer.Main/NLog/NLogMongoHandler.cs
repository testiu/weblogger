﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.LogReceiverService;

namespace LoggerServer.Main.NLog
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class NLogMongoHandler : ILogReceiverServer
    {
        public void ProcessLogMessages(NLogEvents nevents)
        {
            var events = nevents.ToEventInfo("Client.");
            StreamWriter sw=new StreamWriter("testNLog.txt");
            sw.WriteLine(nevents.ClientName);
            sw.Close();
            /*Console.WriteLine("in: {0} {1}", nevents.Events.Length, events.Count);

            foreach (var ev in events)
            {
                var logger = LogManager.GetLogger(ev.LoggerName);
                logger.Log(ev);
            }*/
        }
    }
}
