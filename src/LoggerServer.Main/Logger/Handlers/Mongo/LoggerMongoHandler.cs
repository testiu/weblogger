﻿using System.Diagnostics;
using LogRepository.Entities;
using LogRepository.Repositories;
using Process = LogRepository.Entities.Process;

namespace LoggerServer.Main.Logger.Handlers.Mongo
{
    public class LoggerMongoHandler : Libs.Logger.Net.NetService
    {
        private const int LogPeriod = 2000;
        private IRepository<Log> _repository;
        private LoggerConverter _converter = new LoggerConverter();

        private Libs.Logger.ILogger _mainLogger;

        public LoggerMongoHandler()
        {
            _repository =  new LogMongoDbRepository(new MongoDbRepository<Log>(), new MongoDbRepository<Host>(),
            new MongoDbRepository<Process>());
            _repository.CreateIndex();
        }

        public LoggerMongoHandler(IRepository<Log> repository)
        {
            _repository = repository;
        }

        protected override void OnDataRecieved(Libs.Logger.Common.LoggingEvent data)
        {
            var log = _converter.ConvertToMongoDb(data);

            _repository.Insert(log);
        }
    }
}
