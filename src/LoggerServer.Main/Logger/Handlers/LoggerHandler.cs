﻿using System;

namespace LoggerServer.Main.Logger.Handlers
{
    public class LoggerHandler: Libs.Logger.Net.NetService
    {
        private const int LogPeriod = 2000;

        private Libs.Logger.ILogger _mainLogger;
        private long _receivedMsgCount = 0;

        public LoggerHandler(Libs.Logger.ILogger mainLogger)
        {
            if (mainLogger == null)
                throw new ArgumentNullException("mainLogger");

            _mainLogger = mainLogger;
        }

        protected override void OnDataRecieved(Libs.Logger.Common.LoggingEvent data)
        {
            _mainLogger.Write(data);

            if (System.Threading.Interlocked.Increment(ref _receivedMsgCount) % LogPeriod == 0)
                Logger.Instance.Info("Processed messages count: " + System.Threading.Interlocked.Read(ref _receivedMsgCount).ToString());
        }
    }
}
