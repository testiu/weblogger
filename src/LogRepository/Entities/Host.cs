﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogRepository.Entities
{
    public class Host : EntityBase
    {
        public Host(string host)
        {
            HostName = host;
        }
        public string HostName { get; set; }
    }
}
