﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LogRepository.Entities;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using Process = LogRepository.Entities.Process;

namespace LogRepository.Repositories
{
    public class LogMongoDbRepository: ILogRepository
    {
        private MongoDbRepository<Log> _repositoryLog;
        private MongoDbRepository<Host> _repositoryHost;
        private MongoDbRepository<Process> _repositoryProcess;  

        public LogMongoDbRepository(MongoDbRepository<Log> repositoryLog, MongoDbRepository<Host> repositoryHost,
            MongoDbRepository<Process> repositoryProcess)
        {
            _repositoryLog = repositoryLog;
            _repositoryHost = repositoryHost;
            _repositoryProcess = repositoryProcess;
        }

        public void CreateIndex()
        {
            //_repositoryLog.Collection.DropAllIndexes();
            var keys1 = IndexKeys.Ascending("Host", "ProcessName", "LogLevel").Descending("Date");
            _repositoryLog.Collection.CreateIndex(keys1);

            var keys2 = IndexKeys.Ascending("Host", "ProcessName", "LogLevel");
            _repositoryLog.Collection.CreateIndex(keys2);

            var keys3 = IndexKeys.Descending("Date");
            _repositoryLog.Collection.CreateIndex(keys3);
        }

        public bool Insert(Log entity)
        {
            if (!GetAllHosts().Contains(entity.Host))
                _repositoryHost.Insert(new Host(entity.Host));
            if (!GetAllProcessNames().Contains(entity.ProcessName))
                _repositoryProcess.Insert(new Process(entity.ProcessName));
            return _repositoryLog.Insert(entity);
        }

        public bool Update(Log entity)
        {
            return _repositoryLog.Update(entity);
        }

        public bool Delete(Log entity)
        {
            return _repositoryLog.Delete(entity);
        }

        public IList<Log> SearchFor(Expression<Func<Log, bool>> predicate)
        {
            return _repositoryLog.SearchFor(predicate);
        }

        public IQueryable<Log> AsQueryable()
        {
            return _repositoryLog.AsQueryable();
        }

        public IList<Log> GetAll()
        {
            return _repositoryLog.GetAll();
        }

        public Log GetById(ObjectId id)
        {
            return _repositoryLog.GetById(id);
        }

        public List<string> GetAllHosts()
        {
            return _repositoryHost.GetAll().Select(host => host.HostName).ToList();
        }

        public List<string> GetAllProcessNames()
        {
            return _repositoryProcess.GetAll().Select(proc => proc.ProcessName).ToList();
        }
    }
}
