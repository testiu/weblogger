﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Libs.Logger.Configuration;
using Libs.Logger;
using LoggerServer.Tests.Functional.Logger;
using LoggerServer.Main.Logger.Handlers.Mongo;
using System.Threading;
using System.Collections.Generic;
using LogRepository.Repositories;
using LogRepository.Entities;
using LoggerServer.Tests.Functional;
using MongoDB.Bson;

namespace LoggerServer.Tests.Unit
{
    [TestClass]
    public class MongoOperationsTests : IDisposable
    {
        private bool _initPassed;
        protected IRepository<Log> _repo;
        protected GenerateLogsHelper _logGenerator;

        [TestInitialize]
        public virtual void SetUp()
        {
            if (_initPassed)
                return;

            _initPassed = true;
            _repo = MongoTestHelpers.GetRepository();
            _logGenerator = new GenerateLogsHelper();
        }

        [TestMethod]
        public virtual void Insert()
        {
            List<Log> logList = _logGenerator.GenerateRandomLogs(1);

            var message = logList[0].Message;

            _repo.Insert(logList[0]);

            List<Log> addedLogList = _repo.GetAll().ToList();
            Assert.AreEqual(message, addedLogList[0].Message);
        }

        [TestMethod]
        public virtual void Update()
        {
            List<Log> logList = _logGenerator.GenerateRandomLogs(1);
            _repo.Insert(logList[0]);

            logList[0].Message = "changedMessage";

            _repo.Update(logList[0]);

            List<Log> addedLogList = _repo.GetAll().ToList();
            Assert.AreEqual(logList[0].Message, addedLogList[0].Message);
        }

        [TestMethod]
        public virtual void Delete()
        {
            List<Log> logList = _logGenerator.GenerateRandomLogs(1);
            _repo.Insert(logList[0]);

            List<Log> oldLogList = _repo.GetAll().ToList();
            int oldLogListCount = oldLogList.Count();

            _repo.Delete(oldLogList[0]);

            int newLogListCount = _repo.GetAll().Count();
            Assert.AreNotEqual(oldLogListCount,newLogListCount);
        }

        [TestMethod]
        public virtual void GetAll()
        {
            List<Log> logList = _logGenerator.GenerateRandomLogs(1);
            int oldLogCount = _repo.GetAll().Count();

            _repo.Insert(logList[0]);

            int newLogCount = _repo.GetAll().Count();

            Assert.IsFalse(oldLogCount == newLogCount);
        }

        [TestMethod]
        public virtual void GetLogById()
        {
            List<Log> logList = _logGenerator.GenerateRandomLogs(1);
            _repo.Insert(logList[0]);
            ObjectId id = logList[0].Id;

            Log newLog = _repo.GetById(id);
            Assert.IsNotNull(newLog);
            Assert.AreEqual(id,newLog.Id);
        }

        public void Dispose()
        {
            _repo = MongoTestHelpers.GetRepository();
            _repo = null;
        }
    }
}
