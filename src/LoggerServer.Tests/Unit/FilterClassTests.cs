﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggerServer.Tests.Functional;
using LoggerServer.Tests.Functional.Logger;
using LoggerWebInterface.Controllers;
using LogRepository.Entities;
using LogRepository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoggerServer.Tests.Unit
{
    [TestClass]
    public class FilterClassTests
    {
        private bool _initPassed;
        //protected IRepository<Log> _repo;
        protected GenerateLogsHelper _logGenerator;

        [TestInitialize]
        public virtual void SetUp()
        {
            if (_initPassed)
                return;

            _initPassed = true;
            //_repo = MongoTestHelpers.GetRepository();
            _logGenerator = new GenerateLogsHelper();
        }

        [TestMethod]
        public virtual void FilterLevels()
        {
            List<Log> logList = _logGenerator.GenerateRandomLogs(1000);

            var neededList = logList.Where(p => p.LogLevel == Log.LoggerLevel.Debug);

            var filter = new FilterClass();
            filter.LevelMultiselect.SelectedItems = new List<string>(){"Debug"};
            var resultList = filter.PredicateLevels(logList.AsQueryable());
            
            Assert.AreEqual(neededList.Count(),resultList.Count());
        }

        [TestMethod]
        public virtual void FilterTime()
        {
            List<Log> logList = _logGenerator.GenerateRandomLogs(1000);

            var fromDate = new DateTime(2005, 1, 1);
            var neededList = logList.Where(p => p.Date > fromDate );

            var filter = new FilterClass();
            filter.FromTime = fromDate;
            var resultList = filter.PredicateTime(logList.AsQueryable());

            Assert.AreEqual(neededList.Count(), resultList.Count());
        }
    }
}
