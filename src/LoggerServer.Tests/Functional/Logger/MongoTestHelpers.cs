﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Libs.Logger;
using LogRepository.Entities;
using LogRepository.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;

namespace LoggerServer.Tests.Functional.Logger
{
    public class MongoTestHelpers
    {
        private static Random rand = new Random();
        public static LogMongoDbRepository GetRepository()
        {
            //return new MongoDbRepository<Log>();

            var connectionString = "mongodb://localhost/";
            var dbName = "logtest";
            var collectionLogName = "Log";
            var collectionHostName = "Host";
            var collectionProcName = "Process";

            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            server.DropDatabase(dbName);

            var repoLog = new MongoDbRepository<Log>(connectionString, dbName, collectionLogName);
            var repoHost = new MongoDbRepository<Host>(connectionString, dbName, collectionHostName);
            var repoProc = new MongoDbRepository<Process>(connectionString, dbName, collectionProcName);

            
            return new LogMongoDbRepository(repoLog,repoHost,repoProc);
        }

        public static int CollectionSize(IRepository<Log> repository)
        {
            return repository.GetAll().Count;
        }

        public static bool AssertSizeMatch(int nlogs, IRepository<Log> repository)
        {
            var size = CollectionSize(repository);
            Assert.AreEqual(size,nlogs);
            return true;
        }

        public static bool IsLogsExist(IList<Log> logList, IRepository<Log> repository)
        {
            var logs = repository.GetAll();
            return logList.Select(log => log.Message).All(repository.GetAll().Select(log => log.Message).ToList().Contains) &&
                   //logList.Select(log => log.Exception).All(repository.GetAll().Select(log => log.Exception).ToList().Contains) &&
                   logList.Select(log => log.LogLevel).All(repository.GetAll().Select(log => log.LogLevel).ToList().Contains);
        }

        /*public static List<string> FindLostLogs(IList<Log> logList, IRepository<Log> repository)
        {
            var myLogs = logList.Select(log => log.Message).ToList();
            var mongoLogs = repository.GetAll().Select(log => log.Message).ToList();
            var missedLogs = myLogs.Where(myLog => !mongoLogs.Contains(myLog)).ToList();
            return missedLogs;
        }*/

        public static LogLevel GetLogLevel(Log.LoggerLevel level)
        {
            var mongoLevels = new List<Log.LoggerLevel>()
            {
                Log.LoggerLevel.Trace,
                Log.LoggerLevel.Debug,
                Log.LoggerLevel.Info,
                Log.LoggerLevel.Warn,
                Log.LoggerLevel.Error,
                Log.LoggerLevel.Fatal
            };
            var loggerLevels = new List<LogLevel>()
            {
                LogLevel.Trace,
                LogLevel.Debug,
                LogLevel.Info,
                LogLevel.Warn,
                LogLevel.Error,
                LogLevel.Fatal
            };
            return loggerLevels[mongoLevels.FindIndex(p => p==level)];
        }

        public static string GetRandomMessage()
        {
            int size = 10 + rand.Next(20);
            return GenerateString(size);
        }

        private static string GetRandomException()
        {
            int size = 10 + rand.Next(30);
            return GenerateString(size);
        }

        private static string GenerateString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * rand.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
