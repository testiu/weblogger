﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogRepository.Entities;
using LogRepository.Repositories;

namespace LoggerServer.Tests.Functional
{
    public class GenerateLogsHelper
    {
        private static int logCount = 1000;

        private List<DateTime> timeList = new List<DateTime>(logCount);
        private List<Log.LoggerLevel> levelList = new List<Log.LoggerLevel>() {
            Log.LoggerLevel.Trace,
            Log.LoggerLevel.Debug ,
            Log.LoggerLevel.Info ,
            Log.LoggerLevel.Warn ,
            Log.LoggerLevel.Error ,
            Log.LoggerLevel.Fatal
        };
        private List<string> methodList = new List<string>(400);
        private List<string> classList = new List<string>(40);
        private List<string> nsList = new List<string>(4);
        private List<string> messageList = new List<string>(50);
        private List<string> exceptionList = new List<string>(50);
        private List<string> hostList = new List<string>(50);
        private List<string> stacksourceList = new List<string>(100);
        private List<string> contextList = new List<string>(50);

        private Random random = new Random();

        private List<Log> randomLogs = new List<Log>();

        public List<Log> GenerateRandomLogs(int count)
        {
            logCount = count;

            GenerateAll();

            for (int i = 0; i < logCount; i++)
            {
                randomLogs.Add(GenegateRandomLog());
            }

            return randomLogs;
        }

        private int datenum = 0;
        private int messageNum = 0;
        private Log GenegateRandomLog()
        {
            var level = random.Next(levelList.Count);
            var method = random.Next(methodList.Capacity);
            var clas = random.Next(classList.Capacity);
            var ns = random.Next(nsList.Capacity);
            var message = random.Next(messageList.Capacity);
            var exception = random.Next(exceptionList.Capacity);
            var host = random.Next(hostList.Capacity);
            var stacks = random.Next(stacksourceList.Capacity);
            var context = random.Next(contextList.Capacity);

            var log = new Log
            {
                Date = timeList[datenum++],
                LogLevel = levelList[level],
                //Method = methodList[method],
                //Class = classList[clas],
                //Namespace = nsList[ns],
                Message = messageList[message],//"Log " + messageNum++,
                //Exception = new LogException(),
                //Host = hostList[host],
                //Stacksource = stacksourceList[stacks],
                //Context = contextList[context]
            };

            return log;
        }

        private void GenerateAll()
        {
            GenerateList(timeList, timeList.Capacity, GenerateDateTime);
            timeList = timeList.OrderBy(p => p).ToList();
            GenerateList(methodList, methodList.Capacity, GenerateMethod);
            GenerateList(classList, classList.Capacity, GenerateClass);
            GenerateList(nsList, nsList.Capacity, GenerateNamespace);
            GenerateList(messageList, messageList.Capacity, GenerateMessage);
            GenerateList(exceptionList, exceptionList.Capacity, GenerateException);
            GenerateList(hostList, hostList.Capacity, GenerateHost);
            GenerateList(stacksourceList, stacksourceList.Capacity, GenerateStacksource);
            GenerateList(contextList, contextList.Capacity, GenerateContext);
        }

        private Object GenerateDateTime()
        {
            var year = 2000 + random.Next(15);
            var month = 1 + random.Next(12);
            var day = 1 + random.Next(27);
            return new DateTime(year, month, day);
        }

        private object GenerateContext()
        {
            int size = 20 + random.Next(40);
            return GenerateString(size);
        }

        private object GenerateStacksource()
        {
            int size = 50 + random.Next(50);
            return GenerateString(size);
        }

        private object GenerateException()
        {
            int size = 40 + random.Next(60);
            return GenerateString(size);
        }

        private object GenerateNamespace()
        {
            int size = 20 + random.Next(20);
            return GenerateString(size);
        }

        private object GenerateClass()
        {
            int size = 10 + random.Next(20);
            return GenerateString(size);
        }

        private void GenerateList(IList list, int len, Func<Object> genItem)
        {
            for (int i = 0; i < len; i++)
            {
                list.Add(genItem());
            }
        }

        private Object GenerateMethod()
        {
            int size = 10 + random.Next(20);
            return GenerateString(size);
        }
        private Object GenerateHost()
        {
            int size = 5 + random.Next(10);
            return GenerateString(size);
        }

        private Object GenerateMessage()
        {
            int size = 100 + random.Next(50);
            return GenerateString(size);
        }

        private string GenerateString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}
